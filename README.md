# CS 1440 Assignment 1: Text Tools

* [Instructions](doc/Instructions.md)
* [Expected Output](doc/examples)
* [Hints](doc/Hints.md)
* [Rubric](doc/Rubric.md)


## NOTE
This was an assignment done for CS1440 at Utah State University, much of the project was done by Erik Fowler, but near all of the code was completed by myself. I am only claiming ownership of what I have written, which is all, or near all, of the Python code and some of the doc files. This program is run through tt.py and mimicks many of the built in functions, like cat for example. Run tt.py to see how to work this program. There are included files in the data folder to test the functions on.


## Overview

A client has sought DuckieCorp's services to process a large quantity of textual data.  Specifically, the data is in the form of a Comma Separated Values (CSV) file which is far too large to load as a spreadsheet in an application such as LibreOffice Calc or Microsoft Excel.

I explained to the client that it would be easier for them to install Linux and use its built-in text processing tools for this job.  Fortunately for our bottom line (and your job security), this client is adamant that installing Linux is out of the question.  Instead, DuckieCorp will recreate a suite of Linux text processing programs in Python.

I made a start at this project, but only got as far as creating a general outline and finishing the `usage()` routine before my other responsibilities at DuckieCorp became too great.  I will leave this project to you, my favorite intern, to complete.  If you do good work I am confident that this small job will turn into a standing contract.  



## Objectives

-   Processing command-line arguments in `sys.argv`
-   Organize code into modules
-   Process string data with Python's built-in functionality
-   Efficiently process large quantities of data
-   Use the IDE's interactive debugger


## Expected Behavior

Read the [example](doc/examples) files to see what a correct program's output might look like.  Test your program against the files under `data/` and compare your output to the provided examples.  Your program should match the example output in the essentials; extra leading or trailing white space is generally permissible, except where its presence changes the meaning of the output.

In the provided samples, lines beginning with `$` represent the command I typed into my shell.  When you run the same command you should not copy the `$`.
