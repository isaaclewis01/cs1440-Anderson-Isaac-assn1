Software Development Plan:

    1. Requirements:
        Create a program that creates variations of the Unix text-processing programs in python.

    2. Design:
        This program takes in user input to manipulate files in a certain way. User input should be a file name. If the
        user enters invalid input, the usage function is called for that tool and printing the correct error message.
        Input:
            User enters a command to run tt.py, the tool name, and the file name. Then the program will invoke that tool
            chosen on the chosen file. All input should be strings and spelled correctly.
        Processing:
            When tt.py is called, the input will decide which tool to run. tt.py is used to run tools and forward their
            arguments.
        Output:
            The chosen data file is manipulated using the chosen tool. Some modules, like Concatenate.py, print the
            file. An error message is outputted if the user enters incorrect input.

    3. Implementation:
        This program is entirely located in the src folder, each module being contained in there.
        User is prompted input through the built in input() function.
        User is expected to run this through Git Bash.

    4. Verification:
        Test One:
            Tried to enter CAT instead of cat... not allowed, pulled up usage() and sys.exit.
        Test Two:
            Attempted to enter the file name data/names10... success!
