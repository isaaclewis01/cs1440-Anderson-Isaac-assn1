| Date | Events |
|------|--------|
| January 26 - February 1 | Worked, no time for this assignment |
| February 2 | Cloned repository |
| February 3 | Started my software dev plan and my sprint-signature |
| February 4 | No time to work on project |
| February 5 | Finished my SDP + coded most of the project |
| February 6 | Finished all modules but CutPaste.py|
| February 7 | Finished program! |
