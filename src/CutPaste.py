def cut(args, f=1):
    """remove sections from each line of files"""
    fList = []
    isTooLong = False
    f = sorted(f)
    for fname in args:
        # fList.append(list(f))
        file = open(fname)
        joinedString = ''
        for line in file:

            if len(f) == 1:
                if line.count(',') == int(f[0]) - 1:
                    print(line.split(',')[int(f[0]) - 1], end='')
                elif int(f[0]) - 1 > line.count(','):
                    print('')
                else:
                    print(line.split(',')[int(f[0]) - 1])
            else:
                if int(f[len(f) - 1]) - 1 == line.count(','):
                    isTooLong = True
                else:
                    isTooLong = False
                if int(f[len(f) - 1]) - 1 > line.count(','):
                    print('')
                else:
                    for i in range(int(f[0]) - 1, int(f[len(f) - 2]), int(f[len(f) - 1]) - int(f[len(f) - 2])):
                        #print(f[len(f) - 1])
                        #print(line.count(','))
                        joinedString += line.split(',')[i]
                        joinedString += ','
                    joinedString += line.split(',')[int(f[len(f) - 1]) - 1]

                if not isTooLong:
                    joinedString += '\n'
        print(joinedString, end='')

        file.close()

def paste(args):
    """merge lines of files"""
    fList = []
    for fname in args:
        f = open(fname, "r")
        fList.append(f)

    currentMax = 0
    for file in fList:
        fileSize = 0
        for line in file:
            fileSize += 1

        if fileSize > currentMax:
            currentMax = fileSize

        file.close()

    fList = []
    for fname in args:
        f = open(fname, "r")
        fList.append(f)

    for i in range(currentMax):
        lines = []
        for file in fList:
            lines.append(file.readline().strip("\n"))

        print(",".join(lines), end="")
        print("")

    for file in fList:
        file.close()