#!/usr/bin/env python

from Concatenate import cat, tac
from CutPaste import cut, paste
from Grep import grep
from Grep import startgrep
from Partial import head, tail
from Sorting import sort
from WordCount import wc
from Usage import usage

import sys

counter = 0

if len(sys.argv) < 2:
    usage()
    sys.exit(1)
elif sys.argv[1] == 'cat':
    if len(sys.argv) < 3:
        usage(error='Too few arguments', tool='cat')
    else:
        cat(sys.argv[2:])
elif sys.argv[1] == 'tac':
    if len(sys.argv) < 3:
        usage(error='Too few arguments', tool='tac')
    else:
        tac(sys.argv[2:])
elif sys.argv[1] == 'wc':
    if len(sys.argv) < 3:
        usage(error='Too few arguments', tool='wc')
    else:
        wc(sys.argv[2:])
elif sys.argv[1] == 'head':
    if len(sys.argv) < 3:
        usage(error='Too few arguments', tool='head')
    else:
        if len(sys.argv) < 4 and str(sys.argv[2]) == '-n' or not str(sys.argv[3]).isdigit():
            usage(error='Number of lines is required', tool='head')
        elif str(sys.argv[2]) == '-n':
            head(sys.argv[4:], sys.argv[3])
        else:
            head(sys.argv[2:])
elif sys.argv[1] == 'tail':
    if len(sys.argv) < 3:
        usage(error='Too few arguments', tool='tail')
    else:
        if len(sys.argv) < 4 and str(sys.argv[2]) == '-n' or not str(sys.argv[3]).isdigit():
            usage(error='Number of lines is required', tool='tail')
        elif str(sys.argv[2]) == '-n':
            tail(sys.argv[4:], sys.argv[3])
        else:
            tail(sys.argv[2:])
elif sys.argv[1] == 'grep':
    if len(sys.argv) < 3:
        usage(error='Too few arguments', tool='grep')
    else:
        if str(sys.argv[2]) == '-v':
            grep(sys.argv[3], sys.argv[4:], v='-v')
        else:
            grep(sys.argv[2], sys.argv[3:])
elif sys.argv[1] == 'sort':
    if len(sys.argv) < 3:
        usage(error='Too few arguments', tool='sort')
    else:
        sort(sys.argv[2:])
elif sys.argv[1] == 'paste':
    if len(sys.argv) < 3:
        usage(error='Too few arguments', tool='paste')
    else:
        paste(sys.argv[2:])
elif sys.argv[1] == 'cut':
    if len(sys.argv) < 3:
        usage(error='Too few arguments', tool='cut')
    else:
        if str(sys.argv[2]) == '-f' and len(sys.argv) < 4:
            usage(error="A comma-separated field specification is required", tool='cut')
        elif str(sys.argv[2]) == '-f':
            splitArgs = (sys.argv[3]).split(',')
            for i in range(3, len(sys.argv)):
                if str(sys.argv[i]).isdigit():
                    counter += 1
            cut(sys.argv[4:], f=splitArgs[0:])
        else:
            cut(sys.argv[2:])
elif sys.argv[1] == 'startgrep':
    if len(sys.argv) < 3:
        usage(error='Too few arguments', tool=grep)
    else:
        startgrep(args=sys.argv[2:])
else:
    usage()
    sys.exit(1)
