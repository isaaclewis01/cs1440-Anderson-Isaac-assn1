def cat(args):
    # concatenate files and print on the standard output
    for fname in args:
        f = open(fname)
        for line in f:
            print(line, end='')
        f.close()

def tac(args):
    # concatenate and print files in reverse
    for fname in args:
        f = open(fname)
        for line in reversed(list(f)):
            print(line, end='')
        f.close()