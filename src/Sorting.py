def sort(args):
    """sort lines of text files"""
    newList = []
    for fname in args:
        f = open(fname)
        fList = list(f)
        newList.extend(fList)
        newList.sort()
        f.close()
    for i in range(len(newList)):
        print(newList[i], end='')
