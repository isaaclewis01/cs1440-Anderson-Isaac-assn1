def grep(find, args, v='None'):
    """print lines that match patterns"""
    for fname in args:
        f = open(fname)
        for line in f:
            if v == '-v':
                if find not in line:
                    print(line, end='')
            else:
                if find in line:
                    print(line, end='')
        f.close()

def startgrep(args):
    """print lines of files beginning with a pattern"""
    pattern = args.pop(0)
    for fname in args:
        f = open(fname)
        for line in f:
            if line.startswith(pattern):
                print(line, end='')
        f.close()
