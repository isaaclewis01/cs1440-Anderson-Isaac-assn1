def wc(files):
    """print newline, word, and byte counts for each file"""
    # Init variables for counting newLine, word, and byte
    totalLineCount = 0
    totalWordCount = 0
    totalByteCount = 0
    fileCounter = 0
    # Format variable
    fVal = str(6)

    for fname in files:
        # Reset counter each time
        lineCount = 0
        wordCount = 0
        byteCount = 0
        f = open(fname)
        for line in f:
            for i in range(len(line)):
                if line[i].isspace():
                    wordCount += 1
            lineCount += 1
            byteCount += len(line)

        print(format(str(lineCount), ">" + fVal + "s") + " " + format(str(wordCount),
            ">" + fVal + "s") + " " + format(str(byteCount + lineCount),
            ">" + fVal + "s") + "  " + str(files[fileCounter]))
        # Counters
        fileCounter += 1
        totalLineCount += lineCount
        totalByteCount += byteCount
        totalWordCount += wordCount

        f.close()
    if len(files) > 1:
        print(format(str(totalLineCount), ">" + fVal + "s") + " " + format(str(totalWordCount),
            ">" + fVal + "s") + " " + format(str(totalByteCount + totalLineCount),
            ">" + fVal + "s") + "  total")