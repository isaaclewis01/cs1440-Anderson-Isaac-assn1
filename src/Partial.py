def head(args, n=10):
    """output the first part of files"""
    argCounter = 0
    for fname in args:
        lineCount = 0
        f = open(fname)
        if len(args) > 1:
            print("==> " + args[argCounter] + " <==")
        for line in f:
            if lineCount < int(n):
                print(line, end='')
            else:
                break
            lineCount += 1
        print("")
        argCounter += 1
        f.close()


def tail(args, n=10):
    """output the last part of files"""
    argCounter = 0
    for fname in args:
        f = open(fname)
        if len(args) > 1:
            print("==> " + args[argCounter] + " <==")
        fList = list(f)
        loopStart = len(fList) - int(n)
        if loopStart < 0:
            loopStart = 0
        for i in range(loopStart, len(fList)):
            print(fList[i], end="")
        print("")

        f.close()